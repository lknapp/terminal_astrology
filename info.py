#!/usr/bin/env python
from rich import print
from rich.console import Console

import moon
import zodiac
import weather

width = 50

line = '-'*width

console = Console(width=width)

sign = zodiac.sign_name()
sign_emoji = zodiac.sign_emoji()

phase = moon.phase_name()
phase_emoji = moon.phase_emoji()

weather = weather.current_description()

console.print(f'[bold magenta]{line}[/bold magenta]')
console.print(f'[bold magenta]•••{phase_emoji} {phase} under {sign} {sign_emoji}•••[/bold magenta]', justify = "center")
console.print(f'[bold magenta]{weather}[/bold magenta]', justify = "center")
console.print(f'[bold magenta]{line}[/bold magenta]')
