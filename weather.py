#!/usr/bin/env python
import requests
import os

def current_description():
    api_key = os.getenv('WEATHER_API_KEY')
    url = f'https://api.openweathermap.org/data/2.5/onecall?lat=87%2E67&lon=42&appid={api_key}'
    return requests.get(url).json()['current']['weather'][0]['description']
