#!/usr/bin/env python
from zodiac_sign import get_zodiac_sign
from datetime import date

sign = get_zodiac_sign(date.today())

sign_emoji_map = {
    'Aries':        '♈',
    'Taurus':       '♉',
    'Gemini': 	    '♊',
    'Cancer':       '♋︎',
    'Leo':          '♌',
    'Virgo':        '♍',
    'Libra':        '♎',
    'Scorpio':      '♏',
    'Sagittarius':  '♐',
    'Capricorn':    '♑',
    'Aquarius':     '♒',
    'Pisces':       '♓'
}

def sign_name():
    return sign

def sign_emoji():
    return sign_emoji_map[sign]
