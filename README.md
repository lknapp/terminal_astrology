# Terminal Emoji Astrology

A set of simple command line utilities in Python for rendering
important astrological phenomena in the terminal, such as zodiac sign
and moon phase.

Install dependencies with 'pipenv install -r requirements.txt'

Integrate it into your zsh prompt for a more astrologically informed
coding experience, like:

'''
PHASE=$(python3 ~/projects/astrology/phase.py)
SIGN=$(python3 ~/projects/astrology/sign.py)

PROMPT="${SIGN} ${PHASE}"
'''

For weather to work correctly, you will need an api key from
openweathermap.org set as an environment variable, like:
'''
export WEATHER_API_KEY=<<your weather api key>>
