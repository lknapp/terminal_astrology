#!/usr/bin/env python
import pylunar

mi = pylunar.MoonInfo((41, 52, 32), (-87, 37, 28))
phase_emoji_map = {
    'NEW_MOON':         '🌑',
    'WAXING_CRESCENT':  '🌒',
    'FIRST_QUARTER':    '🌓',
    'WAXING_GIBBOUS':   '🌔',
    'FULL_MOON':        '🌕',
    'WANING_GIBBOUS':   '🌖',
    'LAST_QUARTER':     '🌗',
    'WANING_CRESCENT':  '🌘'
}

def phase_name():
    return mi.phase_name().replace("_", " ").title()

def phase_emoji():
    return phase_emoji_map[mi.phase_name()]
