#!/usr/bin/env python

import moon
import zodiac

print(zodiac.sign_emoji() + " " + moon.phase_emoji())
